package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by Alessio Addimando on 07/03/2017.
 */
public class RangeGeneratorTest {

    public static final int START = 5;
    public static final int STOP = 15;
    public static final int ZERO = 0;
    private AbstractFactory factory = new ConcreteFactory();
    private SequenceNumberStrategy generator = new SequenceNumberStrategy();


    @Before
    public void init() throws Exception {

        this.generator.setStrategyOfGenerator(factory.createRangeGenerator(START,STOP));
    }

    @Test
    public void testGenerationOfSequence() throws Exception {
        for (int i=START; i <= STOP; i++){
            assertTrue(this.generator.next().equals(Optional.of(i)));
        }

    }

    @Test
    public void testReset() throws Exception {

        for(int i = ZERO; i < STOP; i++) {
            this.generator.next();
        }
        this.generator.reset();
        assertTrue(this.generator.next().get().equals(START));
    }

    @Test
    public void testSwitchingToOver() throws Exception {
        for (int i = START; i <= STOP; i++) {
            assertFalse(this.generator.isOver());
            this.generator.next();
        }
        assertFalse(this.generator.next().isPresent());
     }

    @Test
    public void testListOfRemaining() throws Exception {
        List<Integer> listToTest = new ArrayList<>();
        for(int i = this.generator.allRemaining().get(0); i <= STOP; i++){
            listToTest.add(i);
        }
        assertTrue(this.generator.allRemaining().equals(listToTest));
    }

}