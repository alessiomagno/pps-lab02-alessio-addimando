package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by Alessio Addimando on 07/03/2017.
 */
public class RandomGeneratorTest {
        private static final int bitsToGenerate = 5;
        private AbstractFactory factory = new ConcreteFactory();
        private SequenceNumberStrategy generator = new SequenceNumberStrategy();


        @Before
        public void init() throws Exception {

            this.generator.setStrategyOfGenerator(factory.createRandomGenerator(bitsToGenerate));
        }

        @Test
        public void nextIsInRange() throws Exception {
            while(!this.generator.isOver()) {
                Optional<Integer> value = this.generator.next();
                assertTrue((value.equals(Optional.of(0)) || value.equals(Optional.of(1))));

            }
            assertFalse(this.generator.next().isPresent());

        }


        @Test
        public void reset() throws Exception {
            this.generator.next();
            assertNotEquals(bitsToGenerate, getNumberOfIteration() );
            this.generator.reset();
            assertEquals(bitsToGenerate, getNumberOfIteration() );
        }

        @Test
        public void isOver() throws Exception {
            for (int i = 0; i < bitsToGenerate; i++){
                this.generator.next();
            }

            assertTrue(this.generator.isOver());
        }

        @Test
        public void allRemainingTotal() throws Exception {
            assertEquals(bitsToGenerate, this.generator.allRemaining().size());
        }


        private int getNumberOfIteration(){
            int currentIteration = 0;
            while (!this.generator.isOver()){
                currentIteration++;
                this.generator.next();
            }
            return currentIteration;
        }

    }
