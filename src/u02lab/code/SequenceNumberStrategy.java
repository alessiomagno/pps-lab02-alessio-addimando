package u02lab.code;

import java.util.List;
import java.util.Optional;

/**
 * Created by Alessio Addimando on 08/03/2017.
 */
public class SequenceNumberStrategy implements SequenceGenerator {

    SequenceGenerator sequenceGenerator;

    void setStrategyOfGenerator(SequenceGenerator sequenceGenerator ){
        this.sequenceGenerator = sequenceGenerator;
    };

    @Override
    public Optional<Integer> next() {
        return this.sequenceGenerator.next();
    }

    @Override
    public void reset() {
        this.sequenceGenerator.reset();
    }

    @Override
    public boolean isOver() {
        return this.sequenceGenerator.isOver();
    }

    @Override
    public List<Integer> allRemaining() {
        return this.sequenceGenerator.allRemaining();
    }
}
