package u02lab.code;

/**
 * Created by Alessio Addimando on 08/03/2017.
 */
public class ConcreteFactory implements AbstractFactory{

    @Override
    public RangeGenerator createRangeGenerator(int start,int stop ) {
        return new RangeGenerator(start,stop);
    }

    @Override
    public RandomGenerator createRandomGenerator(int sequence) {
        return new RandomGenerator(sequence);
    }
}
