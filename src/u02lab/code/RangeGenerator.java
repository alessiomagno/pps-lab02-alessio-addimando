package u02lab.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {


    private Integer start;
    private Integer stop;
    private Integer currentNumber;
    private boolean over = false;



    public RangeGenerator(int start, int stop){
       this.start = start;
       this.stop = stop;
       this.currentNumber = start;
    }

    @Override
    public Optional<Integer> next() {
        if(isOver())
             return Optional.empty();
        else{
            if(this.currentNumber.equals(this.stop))
                this.over = true;
            return Optional.of(this.currentNumber++);
        }
    }


    @Override
    public void reset() {
        this.currentNumber = this.start;
        this.over = false;
    }

    @Override
    public boolean isOver() {
        return this.over;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> list = new ArrayList<>();
        for(int i = this.currentNumber; i <= this.stop;i++){
            list.add(i);
        }
        return Collections.unmodifiableList(list);
    }


}
