package u02lab.code;

/**
 * Created by Alessio Addimando on 08/03/2017.
 */
public interface AbstractFactory {

    RangeGenerator createRangeGenerator(int start, int stop);

    RandomGenerator createRandomGenerator(int sequence);
}
